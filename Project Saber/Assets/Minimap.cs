﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour
{
    public Transform player;

    void LateUpdate()
    {
        // Follow Player
        Vector3 newPos = player.position;
        newPos.y = transform.position.y;
        transform.position = newPos;

        // Rotate with Player
        transform.rotation = Quaternion.Euler(90f, player.eulerAngles.y, 0f);
    }
}
