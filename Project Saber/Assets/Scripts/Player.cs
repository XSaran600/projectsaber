﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // InputMaster
    InputMaster inputMaster;

    Rigidbody rb;

    public Camera cam;

    // Move
    Vector2 moveInput;

    // Look Direction
    public Vector2 lookPostion;
    bool stopMovement = false;

    public float speed = 10f;

    public float maxHealth = 100f;
    public float currentHealth;

    public Healthbar healthbar;

    void Awake()
    {
        inputMaster = new InputMaster();
        inputMaster.Player.Move.performed += ctx => moveInput = ctx.ReadValue<Vector2>();
        inputMaster.Player.Mouse.performed += ctx => lookPostion = ctx.ReadValue<Vector2>();
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        currentHealth = maxHealth;
        healthbar.SetMaxHealth(maxHealth);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            TakeDamage(20f);
        }
    }

    void TakeDamage(float damage)
    {
        currentHealth -= damage;

        healthbar.SetHealth(currentHealth);
    }

    void FixedUpdate()
    {
        Move();
        Rotate();
    }

    void Move()
    {
        // Move the player
        float h = moveInput.x;
        float v = moveInput.y;



        Vector3 hor = cam.transform.right;
        Vector3 ver = cam.transform.forward;

        hor.y = 0f;
        ver.y = 0f;
        hor.Normalize();
        ver.Normalize();

        hor = hor * h;
        ver = ver * v;

        Vector3 movement = (hor + ver) * speed * Time.deltaTime;
        rb.MovePosition(transform.position + movement);
    }

    void Rotate()
    {
        Vector2 lookInput = moveInput;

        Vector3 hor = cam.transform.right;
        Vector3 ver = cam.transform.forward;

        hor.y = 0f;
        ver.y = 0f;
        hor.Normalize();
        ver.Normalize();

        Vector3 lookDir = (hor * lookInput.x + ver * lookInput.y);
        if (lookDir != Vector3.zero && !stopMovement)
        {
            stopMovement = true;
            rb.MoveRotation(Quaternion.LookRotation(lookDir));
        }
        else
        {
            stopMovement = false;
        }
    }

    void OnEnable()
    {
        inputMaster.Enable();
    }

    void OnDisable()
    {
        inputMaster.Disable();
    }
}
