﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject player;
    Player p;

    public float turnSpeed = 4.0f;
    Vector3 offset;

    void Start()
    {
        p = player.GetComponent<Player>();
        offset = new Vector3(player.transform.position.x, player.transform.position.y + 4.0f, player.transform.position.z - 8.0f);
    }

    void FixedUpdate()
    {
        offset = Quaternion.AngleAxis(p.lookPostion.x * turnSpeed, Vector3.up) * offset;

        transform.position = player.transform.position + offset;

        transform.LookAt(player.transform.position);
    }
}
